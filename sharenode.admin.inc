<?php
/**
 * Imports
 */
require_once 'sharenode.utils.inc';

/**
 * sharenode form
 *
 * @return
 * Associative array containing for creating the form
 */
function sharenode_admin() {
	if (!variable_get('sharenode_site_key', 0)) {
		variable_set('sharenode_site_key', _sharenode_get_site_key());
	}

	global $base_url;

	//need infos for disable or not submit button
	//get the list of the shared sites
	$rows = array();
	$sites_list = sharenode_get_site();

	if (!empty($sites_list)) {
		foreach($sites_list as $site) {
			if (variable_get('parent_site', 0)) {
				$row = array($site->surl, ($site->parent_site) ? 'Parent site' : 'Remote site', l(t('delete'), 'admin/config/services/sharenode/sites/'.$site->sid.'/delete'));
			}
			else {
				$row = array($site->surl, ($site->parent_site) ? 'Parent site' : 'Remote site');
			}
			array_push($rows, $row);
		}
	}

	//Generate key infos for the remote site
	//if this site is already shared, key can't be change
	$form['generate_key'] = array(
		'#type' => 'fieldset',
		'#title' => t('generate key'),
	);

	$form['generate_key']['url_site'] = array(
		'#type' =>	'textfield',
		'#title' => t('Site URL'),
		'#value' => $base_url,
	);

	$form['generate_key']['key_site'] = array(
		'#type' => 'textfield',
		'#title' => t('Site key'),
		'#value' => variable_get('sharenode_site_key', 0),
	);

	$form['generate_key']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Generate new key'),
		'#disabled' => (empty($rows)) ? FALSE : TRUE,

	);

	//declare if the current site is the parent one
	$form['parent_site'] = array(
		'#type' => 'fieldset',
		'#title' => t('Parent site'),
	);

	$form['parent_site']['declare'] = array(
		'#type' => 'checkbox',
		'#title' => t('declare this site as parent one'),
		'#default_value' => variable_get('parent_site', 0),
	);

  $form['parent_site']['is_parent_site'] = array(
    '#type' => 'submit',
    '#value' => t('Validate'),
  );

	//affiche le tableau avec la liste des nodes partagés
	$form['liste_sites'] = array(
		'#type' => 'fieldset',
		'#title' => t('Shared sites'),
	);

	//header for sites list
	if (variable_get('parent_site',0)) {
		$header = array(t('Sites'), t('State'), t('Operations'));
	}
	else {
		$header = array(t('Sites'), t('State'));
	}
	$form['liste_sites']['table_sites'] = array(
		'#markup' => theme('table', array('header'=>$header, 'rows'=>$rows)),
	);

	//form to have new share site
	$form['liste_sites']['add_site'] = array(
		'#type' => 'fieldset',
		'#title' => t('Add distant site'),
	);

	$form['liste_sites']['add_site']['add_site_url'] = array(
		'#type' => 'textfield',
		'#title' => t('URL'),
		'#description' => t('example : http://www.example.com'),
		'#required' => (sharenode_get_site()->fetchCol()) ? FALSE : TRUE,
	);

	$form['liste_sites']['add_site']['add_site_key'] = array(
		'#type' => 'textfield',
		'#title' => t('Key'),
		'#required' => (sharenode_get_site()->fetchCol()) ? FALSE : TRUE,
	);

	$form['liste_sites']['add_site']['add_site_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Add'),
	);

	return $form;
}

/**
 * Add distant site submit
 *
 * @param $form
 * Object form
 * @param $form_state
 * Object form_state
 */
function sharenode_admin_submit($form, &$form_state) {
	//submit values
	switch ($form_state['values']['op']) {
		case $form['liste_sites']['add_site']['add_site_submit']['#value'] :
      if (!empty($form['liste_sites']['add_site']['add_site_url']['#value']) && !empty($form['liste_sites']['add_site']['add_site_key']['#value'])) {
        		_sharenode_add_site( trim($form['liste_sites']['add_site']['add_site_url']['#value']), trim($form['liste_sites']['add_site']['add_site_key']['#value']), $form['parent_site']['declare']['#value']);
      }
      else {
        drupal_set_message(t('Distant site url and/or key are missing.'), $type = 'warning', $repeat = FALSE);
      }
			break;

		case $form['generate_key']['submit']['#value'] :
			variable_set('sharenode_site_key', _sharenode_get_site_key());
			break;

    case $form['parent_site']['is_parent_site']['#value'] :
      if ($form['parent_site']['declare']['#value']) {
        if (!variable_get('parent_site', 0)) {
          variable_set('parent_site', 1);
        }
      }
      else {
          variable_del('parent_site');
      }
      break;
	}
}

/**
 * Delete the current site from sites list and shared nodes in current site
 *
 * @param $sid
 * site id of the site to remove.
 */
function sharenode_admin_delete($sid) {
	//get all node id this site shared with
	$nids = db_select('sharenode_shared', 'ss')
		->fields('ss', array('nid'))
		->condition('sid', $sid)
		->execute()
		->fetchCol();
	$site = sharenode_get_site_by_sid($sid);
	global $base_url;
	$return = FALSE;

	//delete each infos and nodes in remote site
	$url = $site['surl'].'/xmlrpc.php';
	$method_name = 'sharenode.deleteNode';
	$datas['sender_url'] = $base_url;
	foreach($nids as $nid) {
		$node = sharenode_load_sharenode($nid);
		$datas['nuuid'] = $node['nuuid'];
		$params = array(_sharenode_encrypt_datas($site['skey'], $datas));

		//send http request
		$return = xmlrpc($url, array($method_name => $params));
	}

	//Now delete node site infos.
	if (!$return) {
		$params_2 = array(_sharenode_encrypt_datas($site['skey'], $base_url));
		$return_2 = xmlrpc($url, array('sharenode.deleteSite' => $params_2));
	}
	get_xmlrpc_error();

	//delete in own table sharenode_sites
	_sharenode_delete_site($sid);
	drupal_set_message(t('deleted'), 'status', TRUE);

  //delete parent site variable and content type permission if no more shared site
  if (!sharenode_get_site()->fetchCol()) {
    variable_del('parent_site');

    if(sharenode_delete_content_type(sharenode_get_content_type()) > 0) {
      drupal_set_message(t('Can\'t delete content type.'), 'error', FALSE);
    }
  }
	//redirect
	drupal_goto('admin/config/services/sharenode');
}

/**
 * Delete the link between site and node in remote site
 *
 * @param $nid
 * node id
 * @param $sid
 * site id
 *
 */
function sharenode_site_delete($nid, $sid) {
	$sharenode = sharenode_load_sharenode($nid);
	$remote_site = sharenode_get_site_by_sid($sid);

	global $base_url;

	$datas = array();
	$datas['sender_url'] = $base_url;
	$datas['nuuid'] = $sharenode['nuuid'];
	$params = array(_sharenode_encrypt_datas($remote_site['skey'], $datas));

	$url = $remote_site['surl'].'/xmlrpc.php';
	$method_name = 'sharenode.deleteNode';

	// send node for delete to remotes site.
	$return = xmlrpc($url, array($method_name => $params));
	watchdog('sharenode', t('node @node send to remote site', array('@node'=> $sharenode['nid'])), $variables = array(), $severity = WATCHDOG_NOTICE, $link = NULL);

  $error = get_xmlrpc_error();
  if (!$error) {
    //delete infos in sharenode_shared
    if (_delete_sharenode_shared($nid, $sid) > 0) {
      watchdog('sharenode', t('delete share node @node in sharenode_shared', array('@node'=> $nid)), $variables = array('nid' => $nid, 'sid' => $sid), $severity = WATCHDOG_NOTICE, $link = NULL);
    }
    else {
      drupal_set_message(t('Delete error : node @node haven\'t been deleted in sharenode_shared', array('@node'=>$nid)), 'error', TRUE);
    }
   //delete infos in sharenode_node if no more shared sites
    if (count(sharenode_get_sids($nid)) == 0) {
      if (_delete_sharenode_node($nid) > 0) {
        watchdog('sharenode', t('delete share node @node in sharenode_node', array('@node'=> $nid)), $variables = array('nid' => $nid), $severity = WATCHDOG_NOTICE, $link = NULL);
      }
      else {
        drupal_set_message(t('Delete error : node @node haven\'t been deleted in sharenode_node', array('@node'=>$nid)), 'error', TRUE);
      }
    }
  }
	//redirect
	drupal_goto();
}

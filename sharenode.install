<?php
/**
 * Implements hook_uninstall().
 */
function sharenode_uninstall() {
  variable_del('parent_site');
  variable_del('sharenode_site_key');
}

/**
 * Implementation of hook_schema().
 */
function sharenode_schema() {
	//sharing nodes list sites
	$schema['sharenode_sites'] = array(
		'description' => 'The remote sites where nodes can be shared.',
		
		'fields' => array(
			'sid' => array(
				'description' => 'The primary identifier for a site.',
				'type' => 'serial',
				'unsigned' => TRUE,
				'not null' => TRUE
			),
			'surl' => array(
				'description' => 'url of the site.',
				'type' => 'varchar',
				'length' => 255,
				'not null' => TRUE
			),
			'skey' => array(
				'description' => 'Encrypt key for sharing nodes.',
				'type' => 'varchar',
				'length' => 40,
			),
			'parent_site' => array(
				'description' => '1 if this site is the parent one else 0.',
				'type' => 'int',
				'size' => 'tiny',
				'not null' => TRUE,
				'default' => 0,
			),
		),
		
		'unique keys' => array(
		  'url' => array('surl', 'skey'),
		),
		  
		'primary key' => array('sid'),
	);
	
	//content types list allow to share nodes
	$schema['sharenode_content_types'] = array(
		'description' => 'Content types allow to share node.',
		
		'fields' => array(
			'uid' => array(
				'description' => 'id of the shared content type.',
				'type' => 'serial',
				'not null' => TRUE
			),
			'type' => array(
				'description' => 'The machine-readable name of this type.',
				'type' => 'varchar',
				'length' => 32,
				'not null' => TRUE,
			),
			'is_shareable' => array(
				'description' => 'Boolean indicates that the content type is shareable.',
				'type' => 'int',
				'size' => 'tiny',
				'not null' => TRUE,
				'default' => 0,
			),
		),
		
		'unique keys' => array(
		  'type' => array('type'),
		),
		
		'primary key' => array('uid'),
		
		//manque une liaison avec la table node_type
	);
	
	//sharing nodes list
	$schema['sharenode_node'] = array(
		'description' => 'Table containing list of shared nodes.',
	
		'fields' => array(
			'uid' => array(
				'description' => 'Unique id.',
				'type' => 'serial',
				'not null' => TRUE
			),
			'nid' => array(
				'description' => 'Local id of the node.',
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
			),
			'nuuid' => array(
				'description' => 'Node universal unique id.',
				'type' => 'varchar',
				'length' => 64,
				'not null' => TRUE,
			),
			'propagation' => array(
				'description' => 'Boolean indicates if changes on remote sites will take effects',
				'type' => 'int',
				'not null' => TRUE,
				'default' => 0,				
			),
		),
		
		'unique keys' => array(
			'node' => array('nid'),
		),
		
		'primary key' => array('uid'),
	);
	
	$schema['sharenode_shared'] = array(
		'description' => '',
		
		'fields' => array(
			'uid' => array(
				'description' => 'Unique id.',
				'type' => 'serial',
				'not null' => TRUE
			),
			'nid' => array(
				'description' => 'Id of the node.',
				'type' => 'int',
				'not null' => TRUE,
			),
			'sid' => array(
				'description' => 'Id of the site that receive the shared node.',
				'type' => 'int',
				'not null' => TRUE,
			),
		),
		
		'unique keys' => array(
			'shared' => array('nid', 'sid'),
		),
		
		'primary key' => array('uid'),
	);
  
  return $schema;
}
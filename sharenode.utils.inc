<?php
/**
 * utils function for sharenode
 */

/*---------------------- sharenode_site--------------------*/

/**
 * sites list of sharing nodes.
 *
 * @return
 * return an object containing all sites shared with the current one.
 */
function sharenode_get_site() {
	$list = db_select('sharenode_sites', 'ss')
		->fields('ss')
		->orderBy('surl', 'ASC')
		->execute();

	return $list;
}

/**
 * get site with url.
 * 
 * @param $url
 * url of the site 
 *
 * @return
 * Associative array containing site informations.
 */
function sharenode_get_site_by_url($url) {
	$site = db_select('sharenode_sites', 'ss')
		->fields('ss')
		->condition('surl', $url)
		->execute()
		->fetchAssoc();

	return $site;
}

/**
 * get site with sid
 *
 * @param $sid
 * site id of the site
 *
 * @return
 * Associative array containing site informations.
 */
function sharenode_get_site_by_sid($sid) {
	$site = db_select('sharenode_sites', 'ss')
		->fields('ss')
		->condition('sid', $sid)
		->execute()
		->fetchAssoc();
		
	return $site;
}

/**
 * get list of url shared with other sites
 *
 * @param $nid
 * node id
 *
 * @return
 * Associative array containing shared sites id and url
 */
function _sharenode_get_shared_url_list($nid) {
	$list_sid = sharenode_get_sids($nid);
	$liste = array();
	
	if ($list_sid) {		
		//sites list of sites shared with the current node
		$list = db_select('sharenode_sites', 'ss')
				->fields('ss', array('sid', 'surl', 'skey'))
				->condition('sid', $list_sid, 'IN')
				->execute();
		
		foreach($list as $url) {
			$liste[$url->sid] = $url->surl;
		}
	}
	
	return $liste;
}

/**
 * return unshared urls list
 *
 * @param $nid
 * node id
 *
 * @return
 * Associative array containing unshared sites id and url
 */
function _sharenode_get_unshared_url_list($nid) {
	//sites list of sites shared with the current node
	$list_sid = sharenode_get_sids($nid);
	
	if ($list_sid) {
		foreach($list_sid as $sid) {
			$sids[] = $sid;
		}
		
		$list = db_select('sharenode_sites', 'ss')
		->fields('ss', array('sid', 'surl', 'skey'))
		->condition('sid', $sids, 'NOT IN')
		->execute();
	}
	else {
		$list = db_select('sharenode_sites', 'ss')
		->fields('ss', array('sid', 'surl', 'skey'))
		->execute();
	}
	
	
	$liste = array();
	if (!empty($list)) {
		foreach($list as $url) {
			$liste[$url->sid] = $url->surl;
		}
	}
	
	return $liste;
}

/**
 * insert or update a site in table nodeshare_sites
 *
 * @param $url
 * url of the remote site (not null)
 * @param $key
 * generated key of the remote site
 * @param $parent_site
 * boolean indicate if the current site is the parent one.
 *
 * @return
 * the site id of the remote site.
 */
function _sharenode_add_site($url, $key = NULL, $parent_site) {
	//check url existing in database
	$sid = sharenode_get_site_by_url($url);
	//if parent site is check in form and variable 'parent_site' doesn't exist create it 
		if ($parent_site && !variable_get('parent_site', 0)) { 
			variable_set('parent_site', 1);
		}
		
		//if variable 'parent_site' exist insert site are child and vice versa!
		if (variable_get('parent_site', 0)) {
			$parent_site = 0;
		}
		else {
			$parent_site = 1;
		}
	
	if ($sid) { //update infos
		$site = sharenode_get_site_by_sid($sid['sid']);

		db_update('sharenode_sites')
		->fields(array(
			'skey' => $key,
			'parent_site' => $parent_site,
		))
		->condition('sid', $sid)
		->execute();
		drupal_set_message(t('site infos updated'), 'status', TRUE);
	}
	
	else {//insert infos	
		$sid = db_insert('sharenode_sites')
		->fields(array(
			'surl' => $url,
			'skey' => $key,
			'parent_site' => $parent_site,
		))
		->execute();
		
		drupal_set_message(t('site infos saved'), 'status', TRUE);
	}
	return $sid;
}

/**
 * delete sites infos with same site id in sharenode_sites.
 *
 * @param
 * site id
 *
 * @return
 * number of deleted row
 */
function _sharenode_delete_site($sid) {
	$num_deleted	= db_delete('sharenode_sites')
	->condition('sid', $sid)
	->execute();
		
	return $num_deleted;
}

/**
 * generate the site key
 *
 * @return
 * string
 */
function _sharenode_get_site_key(){
	return md5(uniqid(mt_rand(), TRUE));
}

/*---------------------- sharenode_content_type --------------------*/

/**
 * check if content type is shareable
 *
 * @param $type
 * type of content type
 * 
 * @return
 * Boolean
 */
function sharenode_is_shareable_content_type($type) {
	$shareable = sharenode_load_content_type($type);
	return $shareable['is_shareable'];
}

/**
 * load content type infos
 *
 * @param $type
 * type of content type
 * 
 * @return
 * Associative array
 */
function sharenode_load_content_type($type) {
	$content_type = db_select('sharenode_content_types', 'sct')
				->fields('sct', array('type', 'is_shareable'))
				->condition('type', $type)
				->execute()
				->fetchAssoc();
				
	return $content_type;
}

/**
 * Get all shared content type
 *
 * @return
 * Array
 */
function sharenode_get_content_type() {
  $content_type = db_select('sharenode_content_types', 'sct')
    ->fields('sct', array('type'))
    ->execute()
    ->fetchCol();
  return $content_type;
}

/**
 * Delete content type infos
 * 
 * @param $types
 * Array of type
 *
 * @return
 * number of deleted row
 */
function sharenode_delete_content_type($types) {
  $num_deleted = 0;  
 foreach($types as $type) {
    $num_deleted += db_delete('sharenode_content_types')
		->condition('type', $type)
		->execute();
  }
  return;
}

/*---------------------- sharenode_node --------------------*/
/**
 * create a node unique universal id
 *
 * @return
 * string
 */
function _sharenode_create_nuuid() {
    $chars = md5(uniqid(mt_rand(), true));
	
    $uuid  = substr($chars,0,8) . '-';
    $uuid .= substr($chars,8,4) . '-';
    $uuid .= substr($chars,12,4) . '-';
    $uuid .= substr($chars,16,4) . '-';
    $uuid .= substr($chars,20,12);
    return $uuid;
}

/**
 * load the shared node informations by nid
 *
 * @param $nid
 * node id of the shared node
 *
 * @return
 * Associative array containing share node infos
 */
function sharenode_load_sharenode($nid) {
	$node = db_select('sharenode_node', 'sn')
		->fields('sn')
		->condition('nid', $nid)
		->execute()
		->fetchAssoc();
		
	return $node;
}

/**
 * load the shared node by nuuid
 *
 * @param $nuuid
 * node id
 *
 * @return
 * Associative array containing shared node informations
 */
function sharenode_get_node($nuuid) {
	$node = db_select('sharenode_node', 'sn')
		->fields('sn')
		->condition('nuuid', $nuuid)
		->execute()
		->fetchAssoc();
		
	return $node;
}

/**
 * insert datas in sharenode_node
 *
 * @param $nid
 * node id
 * @param $nuuid
 * node unique universal id
 * @param $propagation
 * bolean (not implemented)
 *
 * @return
 * a new InsertQuery object
 */
function _sharenode_insert_node($nid, $nuuid, $propagation = 0) {
	$num_inserted =	db_insert('sharenode_node')
				->fields(array(
					'nid' => $nid,
					'nuuid' => $nuuid,
					'propagation' => $propagation,
				))
				->execute();
				
	return $num_inserted;
}

/**
 * delete datas in sharenode_node
 *
 * @param $nid
 * node id
 *
 * @return
 * number of deleted row
 */
function _delete_sharenode_node($nid) {
	$num_deleted = db_delete('sharenode_node')
		->condition('nid', $nid)
		->execute();
		
	return $num_deleted;
}



/*---------------------- sharenode_shared --------------------*/

/**
 * Ckeck if a node is shared with a site
 *
 * @param $nid
 * node id
 * @param $sid
 * site id
 *
 * @return
 * Associative array containing shared infos 
 */
function sharenode_is_shared($nid, $sid) {
	$is_share = db_select('sharenode_shared', 'ss')
		->fields('ss')
		->condition('nid', $nid)
		->condition('sid', $sid)
		->execute()
		->fetchAssoc();
	
	return $is_share;		
}

/**
	* get sites list the node is shared with
	*
	* @param $nid
	* node id
	*
	* @return
	* Associtive array containing all the site id shared with a node
	*/
function sharenode_get_sids($nid) {
	$sites_list = db_select('sharenode_shared', 'ss')
		->fields('ss', array('sid'))
		->condition('nid', $nid)
		->execute()
		->fetchCol();

	return $sites_list;	
}

/**
 * insert datas in sharenode_shared
 *
 * @param $nid
 * node id
 * @param $sid
 * site id
 *
 * @return
 * a new InsertQuery object
 */
function _sharenode_insert_shared($nid, $sid) {
	$nid =	db_insert('sharenode_shared')
				->fields(array(
					'nid' => $nid,
					'sid' => $sid,
				))
				->execute();
				
	return $nid;
}

/**
 * delete datas in sharenode_shared
 *
 * @param $nid
 * node id
 * @param
 * site id
 *
 * @return
 * number of deleted row
 */
function _delete_sharenode_shared($nid, $sid) {
	$num_deleted = db_delete('sharenode_shared')
		->condition('nid', $nid)
		->condition('sid', $sid)
		->execute();
		
	return $num_deleted;
}

/*---------------------- xmlrpc --------------------*/

/**
 * save or update the node
 *
 * @param $datas
 * Encrypted datas (array : node & sender url)
 */
function sharenode_save($datas) {
	$_datas = _sharenode_decrypt_datas($datas);
	global $base_url;

	$node = $_datas['node'];
	$site = sharenode_get_site_by_url($_datas['sender_url']);
	$node->share_node_select = array($site['sid']);

	unset($node->nid);
	unset($node->vid);
	
	// insert
	try {
		node_save($node);
		watchdog('sharenode', t('node @node create.', array('@node'=>$node->title)), $variables = array($node->nid), $severity = WATCHDOG_NOTICE, $link = NULL);	
	}
	catch(Exception $e) {
		return xmlrpc_error(5, t('Can\'t save node in remote site -> exception : @e.', array('@e'=>$e)));
	}
}

/**
 * update the share node
 *
 * @param $datas
 * Encrypted datas (array : node & sender url)
 */
function sharenode_update($datas) {
	$_datas = _sharenode_decrypt_datas($datas);
	$node = $_datas['node'];
	
	//update
	$sharenode = sharenode_get_node($node->nuuid);
	$node->nid = $sharenode['nid'];
	$node->vid = $sharenode['nid'];

	try{
		node_save($node);
		watchdog('sharenode', t('node @node updated.', array('@node'=>$node->nid)), $variables = array($node->nid), $severity = WATCHDOG_NOTICE, $link = NULL);
	}
	catch(Exception $e){
		return xmlrpc_error(5, t('Can\'t update node in remote site -> exception : @e.', array('@e'=>$e)));
	}
}

/**
 * delete the node
 *
 * @param $datas
 * Encrypted datas (array : node nuuid & sender url)
 */
function sharenode_delete($datas) {
	$_datas = _sharenode_decrypt_datas($datas);
	
	//get sharenode infos
	$sharenode = sharenode_get_node($_datas['nuuid']);
	$site = sharenode_get_site_by_url($_datas['sender_url']);
	
	try {
		node_delete($sharenode['nid']);
		watchdog('sharenode', t('node @node deleted.', array('@node'=>$sharenode['nid'])), $variables = array($sharenode['nid']), $severity = WATCHDOG_NOTICE, $link = NULL);
	}
	catch(Exception $e){
		return xmlrpc_error(5, t('Can\'t delete node in remote site -> exception : @e.', array('@e'=>$e)));
	}
}

/**
 * Delete node informations (share and node) in remote site or parent site
 * @param $datas
 * Encrypted datas (array : node nuuid & sender url)
 * 
 * A priori plus besoin de la fonction

function sharenode_delete_node_info($datas) {
	$_datas = _sharenode_decrypt_datas($datas);
	
	//get sharenode and site infos
	$sharenode = sharenode_get_node($_datas['nuuid']);
	$site = sharenode_get_site_by_url($_datas['sender_url']);
	
	if (_delete_sharenode_shared($sharenode['nid'], $site['sid']) > 0) {
		watchdog('sharenode', t('delete share node @node in sharenode_shared', array('@node'=> $sharenode['nid'])), $variables = array('nid' => $sharenode['nid'], 'sid' => $site['sid']), $severity = WATCHDOG_NOTICE, $link = NULL);
	}
	else{
		return xmlrpc_error(5, t('Delete error : node @node haven\'t been deleted in sharenode_shared', array('@node'=>$sharenode['nid'])), 'status', TRUE);
	}
	//delete only if node isn't shared with other sites
	if (!sharenode_get_sids($sharenode['nid'])){
		if (_delete_sharenode_node($sharenode['nid']) > 0) {
			watchdog('sharenode', t('delete share node @node in sharenode_node', array('@node'=> $sharenode['nid'])), $variables = array('nid' => $sharenode['nid']), $severity = WATCHDOG_NOTICE, $link = NULL);
		}
		else {
			return xmlrpc_error(5, t('Delete error : node @node haven\'t been deleted in sharenode_node', array('@node'=>$sharenode['nid'])), 'status', TRUE);
		}
	}
} */

/**
 * Delete site info in sharenode_sites for remote site
 *
 * @param $datas
 * encrypted datas (string sender_url)
 */
function sharenode_delete_site($datas) {
	$site = sharenode_get_site_by_url(_sharenode_decrypt_datas($datas));
	if (_sharenode_delete_site($site['sid']) > 0) {
		watchdog('sharenode', t('delete infos in sharenode_sites'), $variables = array('url' => $site['surl']), $severity = WATCHDOG_NOTICE, $link = NULL);
	}
	else{
		return xmlrpc_error(5, t('Delete error : site @url haven\'t been deleted in sharenode_sites', array('@url'=>$site['surl'])), 'status', TRUE);
	}

}
	
/**
 * show messages error
 *
 * @return
 * Boolean
 */
function get_xmlrpc_error() {
	if ($error = xmlrpc_error()) {
		drupal_set_message(t('The remote site gave an error : %message, (@code).',
			array(
				'%message' => $error->message,
				'@code' => $error->code
			)
		));
		return true;
	}
	return false;
}

/**
 * Encrypt datas
 *
 * @param $key
 * string key to encrypt datas
 * @param
 * mixed datas to encrypt
 *
 * @return 
 * Encrypted string
 */
function _sharenode_encrypt_datas($key, $datas) {
	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

	$_datas = serialize($datas);
	$_datas = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $_datas, MCRYPT_MODE_ECB, $iv);
	$_datas = bin2hex($_datas);
	
	return $_datas;
}

/**
 * Decrypt datas
 *
 * @param $datas
 * Encrypted datas
 *
 * @return
 * mixed datas
 */
function _sharenode_decrypt_datas($datas) {
	$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	
	$len = strlen($datas);
	$_datas = pack('H'.$len, $datas);
	$_datas = mcrypt_decrypt  (MCRYPT_RIJNDAEL_256, variable_get('sharenode_site_key', 0), $_datas, MCRYPT_MODE_ECB, $iv);
	$_datas = unserialize($_datas);
	
	return $_datas;
}

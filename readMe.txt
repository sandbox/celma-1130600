Sharenode Version D7.

Ce module permet d'échanger un node entre un site Drupal et plusieurs autres sites Drupal.

Mode d'emploi :
	- Installer le module sur le site parent et les autres sites devant recevoir les nodes.
	- Créer les contents types : ils doivent être le même pour le site parent et les sites enfants (1).
	- Pour tout les sites (parent et enfants), aller à "admin/config/services/sharenode" pour créer les liens.
	- Copier/coller l'url et la clef du site parent dans le site enfant et vice versa.
	- Avant de valier ("Add"), ne pas oublier de cocher "declare this site as parent one." sur le site parent.
  - Dans le formulaire du content type partageable du site parent : Cocher la case "Allow to share this content type with an other Drupal Site".
	- Maintenenant il est possible de de partager un node avec un/des site(s) enfant(s).

Infos :
	- Dans un site enfant, il n'est pas possible de modifier ou supprimer un node créer dans le site parent.
	- Détruire un node dans le site parent entraine la destruction du node dans les sites enfants.
	- Actuellement, les taxonomies, les tags et images ne sont pas pris en compte sur les sites enfants (2).
  - La suppression d'un node sur le site enfant se fait depuis le formulaire du node d'origine. Le supprimer de la liste des sites liés.
  -Supprimer le site enfant depuis l'interface d'administration détruit les nodes enfants. S'il n'y a plus de site lié au site parent, tous les settings sont reinitialisés (déclaration du site parent et content_type).
	- Autre point : 
      - les révisions apparaissent bien sur les sites enfants, mais le créateur est inconnu et il n'est pas possible de les supprimer depuis le site parent.
	    - Il est possible de faire des changements de révision depuis le site enfant, en revanche elles ne sont pas prises en compte sur le site parent.

(1). Actuellement il n'est pas possible d'importer un content type sur les sites enfants.
(2). Prochaine étape de développement?
